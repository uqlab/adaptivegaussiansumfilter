## Adaptive Gaussian Sum Filter

This code is Matlab code for solving the problem of adapting the weight of a Gaussian sum in the context of nonlinear filtering, as presented in the following paper.

Gabriel Terejanu, Puneet Singla, Tarunraj Singh, and Peter D. Scott. Adaptive Gaussian sum filter for nonlinear Bayesian estimation. IEEE Transactions on Automatic Control, 56(9):2151�2156, 2011, http://ieeexplore.ieee.org/document/5746510/

## Acknowledgment

This work was partially supported by ONR Contract No. HM1582-08-1-0012 and by the National Science Foundation under Award No. CMMI-0908403.